import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardTemplateComponent } from './components/card-template/card-template.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';



@NgModule({
  declarations: [
    CardTemplateComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [CardTemplateComponent],
})
export class SharedModule { }
