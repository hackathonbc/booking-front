import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateRoomComponent } from '../create-room/create-room.component';
import { MeetingRoom } from '../interface/meeting-room';
import { MeetingRoomService } from '../service/meeting-room.service';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss']
})
export class RoomListComponent implements OnInit {

  dialogRef!: MatDialogRef<CreateRoomComponent, any>;


  displayedColumns: string[] = ['name', 'capacity', 'description', 'ubication', 'actions'];
  dataSource: MeetingRoom[] = [];

  constructor(private meetingRoomService: MeetingRoomService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    //TODO: Eliminar esta linea
    let m = new MeetingRoom();
    m.name = "Sala 1";
    m.capacity = 10;
    m.description = "Sala de reuniones";
    m.ubication = "Edificio A";
    let response: MeetingRoom[] = [];
    response.push(m);
    this.dataSource = response;

    //TODO: Descomentar esta linea
    // this.getMeetingRooms();
  }

  getMeetingRooms(): void {
    this.meetingRoomService.getAllRooms().subscribe({
      next: (response: MeetingRoom[]) => {
        let m = new MeetingRoom();
        m.name = "Sala 1";
        m.capacity = 10;
        m.description = "Sala de reuniones";
        m.ubication = "Edificio A";
        response.push(m);
        this.dataSource = response;

      },
      error: (erro) => {
        console.log(erro)
      }
    }
    );
  }

  openDialog(room?: MeetingRoom): void {
    let rommData = room != null ? room : new MeetingRoom();
    this.dialogRef = this.dialog.open(CreateRoomComponent, {
      maxWidth: '94vw',
      width: '850px',
      height: '85%',
      disableClose: true,
      data: rommData
    });

    this.dialogRef.afterClosed().subscribe(estado=>{
      if(estado) this.getMeetingRooms();
    });
  }



}
