import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MeetingRoom } from '../interface/meeting-room';

@Injectable({
  providedIn: 'root'
})
export class MeetingRoomService {

  apiUrl = environment.REST_API_URL + 'room-hub/meetingroom';

  constructor(private http: HttpClient) { }

  getAllRooms(): Observable<MeetingRoom[]> {
    return this.http.get<MeetingRoom[]>(this.apiUrl);
  }

  getRoomById(id: number): Observable<MeetingRoom> {
    return this.http.get<MeetingRoom>(`${this.apiUrl}/${id}`);
  }

  addRoom(room: MeetingRoom): Observable<MeetingRoom> {
    return this.http.post<MeetingRoom>(this.apiUrl, room);
  }

  updateRoom(room: MeetingRoom): Observable<MeetingRoom> {
    return this.http.put<MeetingRoom>(`${this.apiUrl}/${room.id}`, room);
  }

}
