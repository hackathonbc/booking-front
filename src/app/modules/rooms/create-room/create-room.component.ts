import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { MeetingRoom } from '../interface/meeting-room';
import { MeetingRoomService } from '../service/meeting-room.service';

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.scss']
})
export class CreateRoomComponent implements OnInit {

  roomForm: FormGroup = this.fb.group({
    id: [null],
    name: ['', Validators.required],
    capacity: ['', Validators.required],
    description: ['', Validators.required],
    ubication: ['', Validators.required]
  });

  ubications: string[] = ['Edificio Piramide, Nivel 1', "Edificio Piramide, Nivel 2"];
  action!: 'add' | 'edit' | 'delete' | 'cancel';
  room!: MeetingRoom;

  constructor(
    private meetingRoomService: MeetingRoomService,
    private dialogRef: MatDialogRef<CreateRoomComponent>,
    @Inject(MAT_DIALOG_DATA) private data: MeetingRoom,
    private toast: ToastrService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.setRoom();
  }

  setRoom() {
    this.room = this.data;
    if (this.room.id) {
      this.roomForm.reset({
        ...this.room,
      });
    }
  }

  accept(): void {
    const formValue = { ...this.roomForm.value };
    if (this.roomForm.invalid) {//validamos que no haya error en los campos
      this.roomForm.markAllAsTouched();//simulamos haber tocado los campos para mostrar los errores
      return;
    }
    this.room = formValue;
    (this.room.id) ? this.updateRoom() : this.saveRoom();
  }

  saveRoom(): void {
    this.meetingRoomService.addRoom(this.room).subscribe({
      next: (response: MeetingRoom) => {
        console.log(response);
        this.toast.success('Room created successfully', 'Success');
        this.closeModal(true);
      },
      error: (error) => {
        this.toast.error('An error occurred while creating the room', 'Error');
        console.log(error);
      }
    }
    );
  }

  updateRoom(): void {
    this.meetingRoomService.updateRoom(this.room).subscribe({
      next: (response: MeetingRoom) => {
        console.log(response);
        this.toast.success('Room updated successfully', 'Success');
        this.closeModal(true);
      },
      error: (error) => {
        this.toast.error('An error occurred while updating the room', 'Error');
        console.log(error);
      }
    }
    );
  }

  closeModal(status?: boolean) {
    this.roomForm.reset();
    this.dialogRef.close(status);
  }

  ErrorRequiredMsg(field: string): string {
    const errors = this.roomForm.get(field)?.errors;
    const touch = this.roomForm.get(field)?.touched;

    if ((errors?.['required']) && touch) {
      return `Campo requerido`
    }
    return '';
  }
}
