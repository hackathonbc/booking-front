import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { MakeBooking } from '../../reservations/interface/make-booking';
import { MakeBookingService } from '../../reservations/service/make-booking.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          
          { title: 'Card 1', cols: 2, rows: 1, generalCols:2},
          { title: 'Card 2', cols: 2, rows: 1 , generalCols:2},
          { title: 'Card 3', cols: 2, rows: 2 , generalCols:2},
        ];
      }

      return [
        { title: 'Card 1', cols: 1, rows: 1, generalCols:2 },
        { title: 'Card 2', cols: 1, rows: 1 , generalCols:2},
        { title: 'Card 3', cols: 2, rows: 3, generalCols:2 },
      ];
    })
  );

  displayedColumns: string[] = ['Usuario', 'Sala', 'Fecha', 'Personas', 'Estado', 'Acciones'];
  dataSource: MakeBooking[] = [];

  constructor(private breakpointObserver: BreakpointObserver,
    private makeBookingService: MakeBookingService) {}

    ngOnInit(): void {
      //TODO: Eliminar esta linea
      let m = new MakeBooking();
      m.attendees = 8;
      m.description = "Reunion para presentar el proyecto";
      m.initTime = new Date();
      m.endTime = new Date();
      m.meetingRoom = "Sala 1";
      m.purpose = "Presentacion";
      m.team = "Evolution";
      m.title = "Reunion de presentacion";
      m.usrIdentifier = "CA22301";
      let response: MakeBooking[] = [];
      [1,2,3,4,5,6].forEach(element => {
        m.id = element;
        response.push(m);
      });
      
      this.dataSource = response;
  
      //TODO: Descomentar esta linea
      // this.getMeetingRooms();
    }

}
