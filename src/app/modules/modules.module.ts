import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ToastrModule } from 'ngx-toastr';
import { CalendarComponent } from './reservations/components/calendar/calendar.component';
import { CreateReservastionComponent } from './reservations/create-reservastion/create-reservastion.component';
import { ReservationListComponent } from './reservations/reservation-list/reservation-list.component';
import { CreateRoomComponent } from './rooms/create-room/create-room.component';
import { RoomListComponent } from './rooms/room-list/room-list.component';

import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { SharedModule } from '../shared/shared.module';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { HomeComponent } from './home/home-dash/home.component';


@NgModule({
  declarations: [
    RoomListComponent,
    CreateRoomComponent,
    ReservationListComponent,
    CreateReservastionComponent,
    CalendarComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    AngularMaterialModule,
    SharedModule,
    ToastrModule.forRoot({
      closeButton: true,
      progressBar: true,
    }
    ),
    TimepickerModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class ModulesModule { }
