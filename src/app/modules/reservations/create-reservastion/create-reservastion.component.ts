import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { map } from 'rxjs';

@Component({
  selector: 'app-create-reservastion',
  templateUrl: './create-reservastion.component.html',
  styleUrls: ['./create-reservastion.component.scss']
})
export class CreateReservastionComponent {

  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Card 1', cols: 2, rows: 1, generalCols:2},
          { title: 'Card 2', cols: 2, rows: 1 , generalCols:2},
          { title: 'Card 3', cols: 2, rows: 2 , generalCols:2},
          { title: 'Card 4', cols: 2, rows: 2 , generalCols:2},
          { title: 'Card 5', cols: 2, rows: 2 , generalCols:2},
          { title: 'Card 6', cols: 2, rows: 2 , generalCols:2},
        ];
      }

      return [
        { title: 'Card 1', cols: 3, rows: 2, generalCols:6 },
        { title: 'Card 2', cols: 3, rows: 2 , generalCols:6},
        { title: 'Card 3', cols: 3, rows: 5, generalCols:6 },
        { title: 'Card 4', cols: 3, rows: 2 , generalCols:6},
        { title: 'Card 5', cols: 3, rows: 1, generalCols:6 },
        { title: 'Card 6', cols: 3, rows: 1, generalCols:6 },
        { title: 'Card 7', cols: 3, rows: 1, generalCols:6 },
      ];
    })
  );

  constructor(private breakpointObserver: BreakpointObserver) {}

  selectedDate: Date = new Date();
  selectedTime1: string = '00:00';
  selectedTime2: string = '00:00';

  onTimeSelected1(event: any) {
    this.selectedTime1 = new Date(event).getHours() + ':' + new Date(event).getMinutes();
    
  }

  onTimeSelected2(event: any) {
    this.selectedTime2 = new Date(event).getHours() + ':' + new Date(event).getMinutes();
  }
}
