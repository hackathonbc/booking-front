export class MakeBooking {

    id?: number;
    meetingRoom?: string;
    usrIdentifier?: string;
    team?: string;
    initTime?: Date
    endTime?: Date;
    title?: string;
    description?: string;
    purpose?: string;
    attendees?: number;
}
