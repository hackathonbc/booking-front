import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../modules/home/home-dash/home.component';
import { CreateReservastionComponent } from '../modules/reservations/create-reservastion/create-reservastion.component';
import { ReservationListComponent } from '../modules/reservations/reservation-list/reservation-list.component';
import { RoomListComponent } from '../modules/rooms/room-list/room-list.component';
import { SidenavComponent } from './template/sidenav.component';

const routes: Routes = [
  {
    path: '',
    component: SidenavComponent,
    children: [
      // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'reservation-list', component: ReservationListComponent },
      { path: 'create-reservation', component: CreateReservastionComponent },
      { path: 'room-list', component: RoomListComponent },
      { path: 'home', component: HomeComponent },
      // agrega más rutas con los componentes dinámicos que desees mostrar
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SidenavRoutingModule { }
