import { Component, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  isOpen : boolean = true;
  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
  }

  opened() {
    this.isOpen = !this.isOpen;
  }
}
