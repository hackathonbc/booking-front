import { Component, OnInit } from '@angular/core';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  horaActual!: number;
  greeting!: string;

  constructor() { }

  ngOnInit() {
    this.horaActual = moment().tz('America/El_Salvador').hour();
    this.getGreeting();

  }

  getGreeting(): string {
    if (this.horaActual >= 6 && this.horaActual < 12) {
      this.greeting = "Buenos días";
    } else if (this.horaActual >= 12 && this.horaActual < 19) {
      this.greeting = "Buenas tardes";
    } else {
      this.greeting = "Buenas noches";
    }
    return this.greeting;
  }

}
